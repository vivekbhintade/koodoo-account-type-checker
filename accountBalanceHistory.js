const accountTypeChecker = (accountBalanceHistory) => {
    let result = "A";
    let difference;
    const reverseArr = accountBalanceHistory.slice().reverse();
    reverseArr.slice(1).forEach((eachMonth, index) => {
        if(difference && difference !== (reverseArr[index].account.balance.amount - eachMonth.account.balance.amount))
            result = "B";
        difference = reverseArr[index].account.balance.amount - eachMonth.account.balance.amount;
    });
    return result === "A" ? "A" : "B";
}

const accountBalanceHistory = [
    {
      "monthNumber": 0,
      "account": {
        "balance": { "amount": 0 }
      }
    },
    {
        "monthNumber": 1,
        "account": {
          "balance": { "amount": 120 }
        }
    },
    {
        "monthNumber": 2,
        "account": {
          "balance": { "amount": 200 }
        }
    }
];
console.log(accountTypeChecker(accountBalanceHistory));